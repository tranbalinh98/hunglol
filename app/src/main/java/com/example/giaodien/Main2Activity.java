package com.example.giaodien;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    String apiid = "53fbf527d52d4d773e828243b90c1f8e";
    RecyclerView recyclerView;
    ArrayList<Days> daysArrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getView();
    }

    private void getView() {
        recyclerView = findViewById(R.id.listView2);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        daysArrayList.add(new Days("Mon","17","27","Sunny",R.drawable.rain_040));
        daysArrayList.add(new Days("Tue","17","27","Sunny",R.drawable.rain_040));
        daysArrayList.add(new Days("Wed","17","27","Sunny",R.drawable.rain_040));
        daysArrayList.add(new Days("Thu","17","27","Sunny",R.drawable.rain_040));
        daysArrayList.add(new Days("Fri","17","27","Sunny",R.drawable.rain_040));
        daysArrayList.add(new Days("Sat","17","27","Sunny",R.drawable.rain_040));
        DayAdapter adapter = new DayAdapter(this,daysArrayList);
        recyclerView.setAdapter(adapter);
    }
    public void request7Days(String cityName) {
        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = String.format("http://api.openweathermap.org/data/2.5/forecast/daily?q=%s&units=metric&cnt=7&appid=%s",cityName,apiid);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("Data7Days", response);
                        DataProcessing7Days(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue.add(stringRequest);
    }

    public void DataProcessing7Days(String response) {
        try {
            JSONObject object = new JSONObject(response);
            JSONArray jsonArray = object.getJSONArray("list");
            for (int i = 0; i<7;i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String dt = jsonObject.getString("dt");
                Long l =Long.valueOf(dt);
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("EE, dd MMM");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EE");
                Long s = Long.valueOf(l);
                String date = simpleDateFormat.format(l*1000);

//                //temp
                JSONObject jsonObjectTemp= jsonObject.getJSONObject("temp");
                String min_temp = jsonObjectTemp.getString("min");
                String max_temp = jsonObjectTemp.getString("max");
//                //weather
                JSONArray jsonArrayWeather = jsonObject.getJSONArray("weather");
                JSONObject jsonObjectWeather = jsonArrayWeather.getJSONObject(0);
                String icon = jsonObjectWeather.getString("icon");
                String description = jsonObjectWeather.getString("main");
                String humidity="";
                String wind_speed="";
                if(i==0){
                    String date1 = simpleDateFormat1.format(l*1000);
                    humidity = jsonObject.getString("humidity");
                    wind_speed = jsonObject.getString("speed");
                }
                Log.i("Data7Days","\tDay step  "+i);

                Log.i("Data7Days","Humidity : "+ humidity);
                Log.i("Data7Days","Wind speed : "+ wind_speed);
                Log.i("Data7Days","Day : "+ date);
                Log.i("Data7Days","Min : "+ min_temp);
                Log.i("Data7Days","Max : "+ max_temp);
                Log.i("Data7Days","icon "+icon);
                Log.i("Data7Days","Description "+description);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
