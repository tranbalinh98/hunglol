package com.example.giaodien;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.TypedArrayUtils;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class HoursAdapter extends RecyclerView.Adapter<HoursAdapter.ViewHolder> {

    ArrayList<Hours> hoursArrayList = new ArrayList<>();
    Context context;

    public HoursAdapter(ArrayList<Hours> hoursArrayList, Context context) {
        this.hoursArrayList = hoursArrayList;
        this.context = context;
        Log.i("adapter","onCreate Hours adapter"+hoursArrayList.size());
        Log.i("adapter","hour "+hoursArrayList.get(0).getHour());
        Log.i("adapter","resource "+hoursArrayList.get(0).getResource());
        Log.i("adapter","temp "+hoursArrayList.get(0).getTemp());
    }

    @NonNull
    @Override
    public HoursAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hours,parent,false);
        Log.i("adapter","onCreate called");
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.hour.setText(hoursArrayList.get(position).getHour());
        holder.state.setImageResource(hoursArrayList.get(position).getResource());
        holder.temp.setText(hoursArrayList.get(position).getTemp());

        Log.i("adapter","onBind called");
    }

    @Override
    public int getItemCount() {
        return hoursArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView hour ;
        private ImageView state;
        private TextView temp;
        private CardView cardViewItem;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            hour = itemView.findViewById(R.id.item_hour);
            state = itemView.findViewById(R.id.item_image);
            temp = itemView.findViewById(R.id.item_temp);
            cardViewItem = itemView.findViewById(R.id.cardViewItem);
        }
    }
}
