package com.example.giaodien;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DayAdapter extends RecyclerView.Adapter<DayAdapter.ViewHolder> {
    Context context;
    ArrayList<Days> daysArrayList = new ArrayList<>();

    public DayAdapter(Context context, ArrayList<Days> daysArrayList) {
        this.context = context;
        this.daysArrayList = daysArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_next_days,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtDay.setText(daysArrayList.get(position).getDay());
        holder.txtMinTemp.setText(daysArrayList.get(position).getMinTemp());
        holder.txtMaxTemp.setText(daysArrayList.get(position).getMaxTemp());
        holder.txtStatus.setText(daysArrayList.get(position).getStatus());
        holder.image.setImageResource(daysArrayList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return daysArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtDay,txtMinTemp,txtMaxTemp,txtStatus;
        ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtDay = itemView.findViewById(R.id.item_next_day_day);
            txtMinTemp = itemView.findViewById(R.id.item_next_day_min_temp);
            txtMaxTemp = itemView.findViewById(R.id.item_next_day_max_temp);
            txtStatus = itemView.findViewById(R.id.item_next_day_status);
            image = itemView.findViewById(R.id.item_next_day_day_image);
        }
    }
}
