package com.example.giaodien;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout mDrawer;
    ActionBarDrawerToggle mToggle;
    ArrayList<Hours> hoursArrayList = new ArrayList<>();
    TextView next7days;
    TextView txtCityName,txtState,txtTime,txtTemp,txtDetail,txtWindSpeed,txtHumidity,txtClouds,txtSunrise,txtSunset;
    ImageView image;

    // adapter
    HoursAdapter hoursAdapter;
    String apiid="53fbf527d52d4d773e828243b90c1f8e";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        initWidgetForm();
        initRecyclerView();
        requestToday("Hanoi");

        next7days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Main2Activity.class);
                startActivity(intent);
            }
        });


        mToggle = new ActionBarDrawerToggle(this,mDrawer,R.string.Open,R.string.Close);
        mDrawer.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void initWidgetForm(){
        next7days = findViewById(R.id.btn7DayNext);
        mDrawer = findViewById(R.id.drawer);
        txtCityName = findViewById(R.id.txtCityName);
        txtState = findViewById(R.id.txtState);
        txtTime = findViewById(R.id.txtTime);
        txtTemp = findViewById(R.id.txtTemp);
        txtDetail = findViewById(R.id.txtDetail);
        txtWindSpeed =findViewById(R.id.txtWindSpeed);
        txtHumidity = findViewById(R.id.txtHumidity);
        txtClouds =findViewById(R.id.txtClouds);
        txtSunrise = findViewById(R.id.txtSunrise);
        txtSunset = findViewById(R.id.txtSunset);
        image = findViewById(R.id.image);
    }
    private void initData() {
        hoursArrayList.add(new Hours("18:00",R.drawable.rain_1_038,"25°C"));
//        hoursArrayList.add(new Hours("19:00",R.drawable.rain_1_038,"27°C"));
//        hoursArrayList.add(new Hours("20:00",R.drawable.rain_1_038,"28°C"));
//        hoursArrayList.add(new Hours("21:00",R.drawable.rain_1_038,"22°C"));
//        hoursArrayList.add(new Hours("22:00",R.drawable.rain_1_038,"20°C"));
//        hoursArrayList.add(new Hours("23:00",R.drawable.rain_1_038,"19°C"));
//        hoursArrayList.add(new Hours("24:00",R.drawable.rain_1_038,"15°C"));
//        hoursArrayList.add(new Hours("1:00",R.drawable.rain_1_038,"15°C"));
//        hoursArrayList.add(new Hours("2:00",R.drawable.rain_1_038,"15°C"));
//        hoursArrayList.add(new Hours("3:00",R.drawable.rain_1_038,"15°C"));
//        hoursArrayList.add(new Hours("4:00",R.drawable.rain_1_038,"15°C"));
//        hoursArrayList.add(new Hours("5:00",R.drawable.rain_1_038,"15°C"));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search,menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.item_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                requestToday(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if(id == R.id.dash){
            Toast.makeText(this, "Dash", Toast.LENGTH_SHORT).show();
        }if(id == R.id.dash1){
            Toast.makeText(this, "Dash1", Toast.LENGTH_SHORT).show();
        }if(id == R.id.dash2){
            Toast.makeText(this, "Dash2", Toast.LENGTH_SHORT).show();
        }if(id == R.id.dash3){
            Toast.makeText(this, "Dash3", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public void dialog(){
        AlertDialog.Builder alerBuilder = new AlertDialog.Builder(this);
        alerBuilder.setTitle("Thong bao");
        alerBuilder.setIcon(R.drawable.rain_1_038);
        alerBuilder.setMessage("Đây là thông tin 7 ngày tiếp theo");
        alerBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(MainActivity.this,Main2Activity.class);
                startActivity(intent);
            }
        });
        alerBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alerBuilder.show();
    }
    public void initRecyclerView(){
        Log.i("adapter","init recycler");
        LinearLayoutManager linearLayout = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL,false);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayout);
        hoursAdapter = new HoursAdapter(hoursArrayList,MainActivity.this);
        recyclerView.setAdapter(hoursAdapter);
        hoursAdapter.notifyDataSetChanged();
    }
    public void requestToday(String cityName){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = String.format("https://api.openweathermap.org/data/2.5/weather?q=%s&units=metric&appid=%s",cityName,apiid);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("Today",response);
                        DataProcessingToday(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue.add(stringRequest);
    }
    public void DataProcessingToday(String response){
        try {
            JSONObject object = new JSONObject(response);
            //coord
            JSONObject jsonObjectCoord = object.getJSONObject("coord");
            String lon = jsonObjectCoord.getString("lon");
            String lat = jsonObjectCoord.getString("lat");
            Log.i("Today","Lon : "+lon);
            Log.i("Today","Lat : "+lat);

            //weather
            JSONArray jsonArray = object.getJSONArray("weather");
            JSONObject jsonObjectWeather = jsonArray.getJSONObject(0);
            String main = jsonObjectWeather.getString("main");
            String icon = jsonObjectWeather.getString("icon");
            Log.i("Today","Main : "+main);
            Log.i("Today","Icon : "+icon);
            //main

            JSONObject jsonObjectMain = object.getJSONObject("main");
            String temp = jsonObjectMain.getString("temp");
            String min_temp = jsonObjectMain.getString("temp_min");
            String max_temp = jsonObjectMain.getString("temp_max");
            String feels_like = jsonObjectMain.getString("feels_like");
            String humidity = jsonObjectMain.getString("humidity");
            String pressure = jsonObjectMain.getString("pressure");
            Log.i("Today","Temp : "+temp);
            Log.i("Today","Max : "+max_temp);
            Log.i("Today","Min : "+min_temp);
            Log.i("Today","Feels : "+feels_like);
            Log.i("Today","Humidity : "+humidity);
            Log.i("Today","Pressure : "+pressure);
            //clouds
            JSONObject jsonObjectCloud = object.getJSONObject("clouds");
            String cloud = jsonObjectCloud.getString("all");

            String cityName = object.getString("name");
            String d = object.getString("dt");
            Long l = Long.valueOf(d);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE dd-MM-yyyy mm-HH");
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("EE, dd MMM");
            String date = simpleDateFormat.format(l*1000L);
            String time = simpleDateFormat1.format(l*1000L);
            Log.i("Today","City : "+cityName);
            Log.i("Today","Date : "+date);
            //sys
            JSONObject jsonObjectSys = object.getJSONObject("sys");
            String state = jsonObjectSys.getString("country");
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("HH:mm");
            String s1 = jsonObjectSys.getString("sunrise");
            String s2 = jsonObjectSys.getString("sunset");
            long l1 = Long.valueOf(s1);
            long l2 = Long.valueOf(s2);
            String sunrise = simpleDateFormat2.format(l1*1000L);
            String sunset = simpleDateFormat2.format(l2*1000L);
            //wind
            JSONObject jsonObjectWind = object.getJSONObject("wind");
            String wind_speed = jsonObjectWind.getString("speed");
//             set data to Form
            txtCityName.setText(cityName);
            txtState.setText(changeCountry(state));
            txtTime.setText(time);
            txtTemp.setText(temp);
            txtDetail.setText(main);
            txtWindSpeed.setText(wind_speed);
            txtHumidity.setText(humidity);
            image.setImageResource(changeIcon(icon));
            txtClouds.setText(cloud);
            txtSunrise.setText(sunrise);
            txtSunset.setText(sunset);
//            call to requestHourly
            requestHourly(lat,lon);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void requestHourly(String lat, String lon) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = String.format("https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&exclude=daily&units=metric&appid=%s", lat, lon, apiid);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("Request", response);
                        DataHourlyProcessing(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Request", "error");
                    }
                });
        requestQueue.add(stringRequest);
    }
    public void DataHourlyProcessing(String response) {
        try {
            JSONObject object = new JSONObject(response);
            JSONArray jsonArrayHourly = object.getJSONArray("hourly");
            for (int i = 0; i < 24; i++) {
                JSONObject jsonObject = jsonArrayHourly.getJSONObject(i);
                String temp = jsonObject.getString("temp");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH");
                String datetime = jsonObject.getString("dt");
                Long l = Long.valueOf(datetime);
                String hour = simpleDateFormat.format(l * 1000);

                JSONArray jsonArrayWeather = jsonObject.getJSONArray("weather");
                JSONObject jsonObject1 = jsonArrayWeather.getJSONObject(0);
                String icon = jsonObject1.getString("icon");
                Log.i("Data", "Day " + i);
                Log.i("Data", temp);
                Log.i("Data", hour);
                Log.i("Data", icon);
                Hours hours = new Hours(hour+":00",changeIcon(icon),temp+"°C");
                hoursArrayList.add(hours);

            }
            hoursAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public static int changeIcon(String icon) {
        switch (icon) {
            case "01n": {
                return R.drawable.moon_034;
            }
            case "01d": {
                return R.drawable.sun_050;

            }
            case "02d": {
                return R.drawable.clouds_049;
            }
            case "02n": {
                return R.drawable.clouds_049;

            }
            case "03d": {
                return R.drawable.cloudy_003;

            }
            case "03n": {
                return R.drawable.moon_3_020;

            }
            case "04d": {
                return R.drawable.cloudy_003;

            }
            case "04n": {
                //043
                return R.drawable.cloudy_043;

            }
            case "09d": {
                //040
                return R.drawable.rain_040;

            }
            case "09n": {
                //040
                return R.drawable.rain_040;

            }
            case "10d": {
                //038
                return R.drawable.rain_1_038;

            }
            case "10n": {
                return R.drawable.night_rain_011;

            }
            case "11d": {
                //041
                return R.drawable.storm_041;

            }
            case "11n": {
                //041
                return R.drawable.storm_041;

            }
            case "13d": {
                //042
                return R.drawable.snow_042;

            }
            case "13n": {
                //042
                return R.drawable.snow_042;

            }
            case "50d": {
                //030
                return R.drawable.clouds_1_030;

            }
            case "50n": {
                //030
                return R.drawable.clouds_1_030;

            }

        }
        return R.drawable.sun_050;
    }
    public static String changeCountry(String country){
        if(country.equals("VN")){
            return "Việt Nam";
        }
        return country;
    }
}
