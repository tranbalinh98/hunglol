package com.example.giaodien;

public class Days {
    private String day;
    private String minTemp;
    private String maxTemp;
    private String status;
    private int image;

    public Days(String day, String minTemp, String maxTemp, String status, int image) {
        this.day = day;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.status = status;
        this.image = image;
    }

    public Days() {
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(String minTemp) {
        this.minTemp = minTemp;
    }

    public String getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(String maxTemp) {
        this.maxTemp = maxTemp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
