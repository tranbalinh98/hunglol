package com.example.giaodien;

public class Hours {
    private int resource;
    private String hour;
    private String temp;

    public Hours(String hour,int resourc,String temp) {
        this.resource = resourc;
        this.hour = hour;
        this.temp = temp;
    }

    public Hours() {
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resourc) {
        this.resource = resourc;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
